import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HeaderService } from 'src/app/service/header.service';

@Component({
  selector: 'app-page5',
  templateUrl: './page5.component.html',
  styleUrls: ['./page5.component.scss']
})
export class Page5Component implements OnInit {


  titre: string = "step 5";
  step1: boolean = true;
  step2: boolean = true;
  step3: boolean = true;
  step4: boolean = true;
  step5: boolean = true;

  liste: boolean[] = [];

  constructor(private headerService: HeaderService, private router: Router) { }

  ngOnInit(): void {
    this.headerService.changeTitre(this.titre);
    this.liste = [this.step1, this.step2, this.step3, this.step4, this.step5];
  }


  previous() {

    this.router.navigate(["/page4"])

  }

  next() {
    this.router.navigate(["/page5"])
  }

}
