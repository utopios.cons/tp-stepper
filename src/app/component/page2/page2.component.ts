import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HeaderService } from 'src/app/service/header.service';

@Component({
  selector: 'app-page2',
  templateUrl: './page2.component.html',
  styleUrls: ['./page2.component.scss']
})
export class Page2Component implements OnInit {

  titre: string = "step 2";
  step1: boolean = true;
  step2: boolean = true;
  step3: boolean = false;
  step4: boolean = false;
  step5: boolean = false;

  liste: boolean[] = [];

  constructor(private headerService: HeaderService, private router: Router) { }

  ngOnInit(): void {
    this.headerService.changeTitre(this.titre);
    this.liste = [this.step1, this.step2, this.step3, this.step4, this.step5];
  }


  previous() {

    this.router.navigate(["/page1"])

  }

  next() {
    this.router.navigate(["/page3"])
  }

}
