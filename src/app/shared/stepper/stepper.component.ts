import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-stepper',
  templateUrl: './stepper.component.html',
  styleUrls: ['./stepper.component.scss']
})
export class StepperComponent implements OnInit {

  step1: boolean = true;
  step2: boolean = true;
  step3: boolean = true;
  step4: boolean = true;
  step5: boolean = true;

  isLinear = true;

  @Input()
  listeStep: boolean[] | undefined;

  constructor() { }

  ngOnInit(): void {

  this.step1 = this.listeStep[0];
  this.step2 = this.listeStep[1];
  this.step3 = this.listeStep[2];
  this.step4 = this.listeStep[3];
  this.step5 = this.listeStep[4];
  }

  class="mat-horizontal-stepper-wrapper ng-tns-c46-0 ng-star-inserted"

}
